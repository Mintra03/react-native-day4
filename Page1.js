import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

class Page1 extends Component {
    render() {
        const { todos, completes, addTodo } = this.props // เรียกว่า deconstructing
        console.log(todos)
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => {
                    addTodo(1);
                    
                }}>
                    <Text> Add Number To Todo </Text>
                </TouchableOpacity>
                <Text>
                    Lastest todo is : {todos[todos.length - 1]}
                </Text>
            </View>
        )
    }
}

const mapStateToProps = (state) => {  // subscribe
    return {
        todos: state.todos,
        completes: state.completes
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addTodo: (topic) => {
            dispatch({
                type: 'ADD_TODO',
                topic: topic
            })
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Page1)