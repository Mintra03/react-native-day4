export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return [...state, action.topic]
        case 'ROMOVE_TODO':
            return state.filter((item, index) => index !== action.index)
        default:
            return state
    }
}