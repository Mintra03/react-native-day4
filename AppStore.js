import { createStore, combineReducers } from 'redux';
import TodosReducer from './TodosReducer';
import completesReducers from './CompletesReducer'

const reducer = combineReducers({
    todos: TodosReducer,
    completes: completesReducers
})

const store = createStore(reducer);
export default store;